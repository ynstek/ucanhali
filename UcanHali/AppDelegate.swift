//
//  AppDelegate.swift
//  UcanHali
//
//  Created by Emre Özdil on 23/08/2017.
//  Copyright © 2017 Emre Özdil. All rights reserved.
//

import UIKit
import Alamofire
import Mantle

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func bindingData() {
        for airport in GlobalVariables.shared.airports! {
            let port = GlobalVariables.shared.portList!.filter() {
                ports in
                return ports.Code == airport.DestinationLocation && ports.Coordinate != nil && ports.Coordinate!.latitude != nil
            }
            if port.first != nil {
                airport.Coordinate = port.first!.Coordinate!
            }
        }
        
        GlobalVariables.shared.airports = GlobalVariables.shared.airports!.filter() {
            airport in
            return airport.Coordinate.latitude != nil
        }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func getAirports() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        //        let api = "https://api-crt.cert.havail.sabre.com/v2/shop/flights/fares?origin=IST&lengthofstay=0&location=TR&pointofsalecountry=TR"
        let api = "https://api-crt.cert.havail.sabre.com/v2/shop/flights/fares?origin=IST&lengthofstay=0&pointofsalecountry=TR"
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer T1RLAQJ1i34xDmGuMeBEBsxoOC54u8vnPBBk3YDslE+wl/nPSNGa8XrhAADA4jw0eXGcAUXgGRPqeb3nyPDEWFGbT6vRcSPxPFNQthbqZonMlr40j24cAq/mTw0b4zy1xXNzYEUVM1Swth6D6M1Yoh+fZB1coS5OU4bsAhXC46QGFKHtj+BBfR8pxbawMCuSxufBfL6Lp4cka7yU9gp2lH/UZ4f75CanwZZtPf10swyRMMR0LQ3CT6KQDpYxraLMjQ5f68bwQ6BcR3QhiW8SRDObubO919N869/XHyZ8wzpJCZrFdGTh4HiIkp1G",
            "Accept": "application/json"
        ]
        
        Alamofire.request(api, headers: headers).responseJSON(completionHandler: { (response) in
            let value = (response.result.value as? [AnyHashable: Any]) //.flatMap { return $0 }
//            value?.removeValue(forKey: AnyHashable("N/A"))
            
            if let jsonDictionary = value  {
                let model: AirportsResponseModel = try! MTLJSONAdapter.model(of: AirportsResponseModel.self, fromJSONDictionary: jsonDictionary, error: (
                    
                )) as! AirportsResponseModel
                
                GlobalVariables.shared.airports = model.FareInfo!.filter() {
                    model in
                    return model.LowestFare != nil
                }
                self.getPorts()
            }
        })
    }
    
    func getPorts() {
        let api = "https://api.turkishairlines.com/test/getPortList?airlineCode=TK&languageCode=TR"
        
        let headers: HTTPHeaders = [
            "apisecret": "885c340e96ac4c7a9638c021ccbe8a01",
            "apikey": "l7xxf90f2f436d3b48bba2a0d0ef5aec7008"
        ]
        
        Alamofire.request(api, headers: headers).responseJSON(completionHandler: { (response) in
            if let jsonDictionary = response.result.value as? [AnyHashable: Any] {
                let model: PortsResponseModel = try! MTLJSONAdapter.model(of: PortsResponseModel.self, fromJSONDictionary: jsonDictionary, error: (
                    
                )) as! PortsResponseModel
                
                GlobalVariables.shared.portList = model.data!.Port
                self.bindingData()
            }
        })
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        self.getAirports()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

