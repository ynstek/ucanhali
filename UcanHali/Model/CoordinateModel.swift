//
//  CoordinateModel.swift
//  UcanHali
//
//  Created by Yunus TEK on 22.09.2018.
//  Copyright © 2018 Emre Özdil. All rights reserved.
//

import Foundation

class CoordinateModel : UHModel {
    var latitude : String?
    var longitude : String?
}
