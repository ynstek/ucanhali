//
//  PortDataModel.swift
//  UcanHali
//
//  Created by Yunus TEK on 22.09.2018.
//  Copyright © 2018 Emre Özdil. All rights reserved.
//

import Foundation

class PortDataModel : UHModel {
    var Port : [PortModel]?
    
    class func PortJSONTransformer() -> ValueTransformer {
        return ValueTransformer.mtl_JSONArrayTransformer(withModelClass: PortModel.self)
    }
}
