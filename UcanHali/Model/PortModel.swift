//
//  PortModel.swift
//  UcanHali
//
//  Created by Yunus TEK on 22.09.2018.
//  Copyright © 2018 Emre Özdil. All rights reserved.
//

import Foundation

class PortModel : UHModel {
    var Code : String?
    var Coordinate : CoordinateModel?
    
    class func CoordinateJSONTransformer() -> ValueTransformer {
        return ValueTransformer.mtl_JSONDictionaryTransformer(withModelClass: CoordinateModel.self)
    }
}
