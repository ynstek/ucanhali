//
//  AirportsModel.swift
//  UcanHali
//
//  Created by Yunus TEK on 18.09.2018.
//  Copyright © 2018 Yunus TEK. All rights reserved.
//

import Foundation
import Mantle

class FareInfoModel: UHModel {
    var name : String?
    var DestinationLocation : String?
    var Distance : Int = 0
    var Coordinate = CoordinateModel()
    var LowestFare: LowestFareModel?
    
//    public static func LowestFareJSONTransformer() -> ValueTransformer {
//        return MTLValueTransformer.init(usingForwardBlock: { (JSONDictionary, nil, error) -> Any? in
//            if ((JSONDictionary as? String) != nil) {
//                return nil
//            }
//            return MTLJSONAdapter.dictionaryTransformer(withModelClass: LowestFareModel.self)
//        })
//    }

    class func LowestFareJSONTransformer() -> ValueTransformer? {
        return MTLValueTransformer.init(block: { (value) -> Any? in
            if (value as? String) != nil {
                return nil
            }
                return ValueTransformer.mtl_JSONDictionaryTransformer(withModelClass: LowestFareModel.self)
        })
    }
    
    
    class func CoordinateJSONTransformer() -> ValueTransformer {
        return ValueTransformer.mtl_JSONDictionaryTransformer(withModelClass: CoordinateModel.self)
    }
    
}
