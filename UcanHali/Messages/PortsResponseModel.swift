//
//  PortsResponseModel.swift
//  UcanHali
//
//  Created by Yunus TEK on 22.09.2018.
//  Copyright © 2018 Emre Özdil. All rights reserved.
//

import UIKit
import Foundation
import Mantle

class PortsResponseModel: UHResponseModel {
    var data : PortDataModel?
    
    class func dataJSONTransformer() -> ValueTransformer {
        return ValueTransformer.mtl_JSONDictionaryTransformer(withModelClass: PortDataModel.self)
    }
}
