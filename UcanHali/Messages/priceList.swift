//
//  PriceList.swift
//  UcanHali
//
//  Created by Yunus TEK on 23.09.2018.
//  Copyright © 2018 Emre Özdil. All rights reserved.
//

import Foundation
import Mantle

struct PriceList {
    var price : Int?
    
    init(price: Int) {
        self.price = price
    }    
}
