//
//  AirportResponseModel.swift
//  UcanHali
//
//  Created by Yunus TEK on 18.09.2018.
//  Copyright © 2018 Yunus TEK. All rights reserved.
//

import UIKit
import Foundation
import Mantle

class AirportsResponseModel: UHResponseModel {
    var FareInfo : [FareInfoModel]?
    
    class func FareInfoJSONTransformer() -> ValueTransformer {
        return ValueTransformer.mtl_JSONArrayTransformer(withModelClass: FareInfoModel.self)
    }
}
