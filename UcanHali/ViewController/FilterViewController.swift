//
//  FilterViewController.swift
//  UcanHali
//
//  Created by Yunus TEK on 23.09.2018.
//  Copyright © 2018 Emre Özdil. All rights reserved.
//

import UIKit

class FilterViewController: ViewController {

    @IBOutlet var butceLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.navigationBar.backgroundColor = UIColor("#EF2535")
        navigationController?.navigationBar.tintColor = UIColor.white
        
        self.title = "Filtre"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }

    @IBAction func butceChange(_ sender: UISlider) {
        butceLabel.text = String(format: "%.0f", sender.value) + " TL"
    }
    
    @IBAction func ucusuGoster(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func viewClick(_ sender: UIView) {
       self.view.endEditing(true)
    }
}
