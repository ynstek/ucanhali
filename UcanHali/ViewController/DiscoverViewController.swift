//
//  DiscoverViewController.swift
//  UcanHali
//
//  Created by Yunus TEK on 20.09.2018.
//  Copyright © 2018 Yunus TEK. All rights reserved.
//

import UIKit
import MapKit

class DiscoverViewController: ViewController {

    @IBOutlet var priceCollectionView: UICollectionView!
    @IBOutlet var mapView: MKMapView!
    let initialLocation = CLLocation(latitude: 41.015137, longitude: 28.979530)
    let regionRadius: CLLocationDistance = 8000000
    var artworks: [Artwork] = []
    var prices: [PriceList] = []
    @IBOutlet var detailView: UIView!
    
    @IBOutlet var subTitleLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.backgroundColor = UIColor("#EF2535")
        navigationController?.navigationBar.tintColor = UIColor.white
        
        self.title = "SAW, İstanbul\nkalkışlı uçuşlar"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    func prepareUI() {
        priceCollectionView.delegate = self
        self.detailView.frame.origin = CGPoint(x: 16, y: 1000)

        // MapKit
        mapView.delegate = self
        self.centerMapOnLocation(location: initialLocation)
        
        loadInitialData()
        mapView.addAnnotations(artworks)
    }
    
    func loadInitialData() {
        var fiyat = 100.00
        for airport in GlobalVariables.shared.airports! {
            //            fiyat = airport.LowestFare!.Fare
            fiyat += Double(arc4random_uniform(42))
            
            var name = airport.DestinationLocation!
            
            switch name {
            case "ADA":
                name = "Adana,\nSakirpasa Airport"
            case "CAI":
                name = "Cairo,\nCairo International Airport"
            case "VIE":
                name = "Klein-Neusiedl,\nVienna Schwechat International Airport"
            case "MCT":
                name = "Muscat,\nSeeb International Airport"
            case "BCN" :
                name = "El Prat de Llobregat,\nBarcelona International Airport"
            case "AMS" :
                name = "Schiphol Zuid,\nSchiphol Airport"
            case "BKK" :
                name = "Lak Si,\nBangkok International Airport"
            default:
                break
            }
            
            artworks.append(Artwork(title: String(format:"%.1f TL", fiyat) , locationName: name, discipline: "", coordinate: CLLocationCoordinate2DMake(Double(airport.Coordinate.latitude!)!, Double(airport.Coordinate.longitude!)!)))
        }
    }
}

extension DiscoverViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? Artwork else { return nil }
        let identifier = "marker"
        if #available(iOS 11.0, *) {
            var view: MKMarkerAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
                as? MKMarkerAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
                view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            }
            return view
        } else {
            return nil
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {

        if (view.annotation != nil) {
            self.titleLabel.text = (view.annotation!.title as! String) + " " + (view.annotation!.subtitle as! String)
            
        }
        if self.detailView.frame.origin == CGPoint(x: 16, y: 523) {
                UIView.animate(withDuration: 0.3, animations: {
                    self.detailView.frame.origin = CGPoint(x: 16, y: 0)
                })
        } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.detailView.frame.origin = CGPoint(x: 16, y: 523)
                })
        }
        
    }
    
}

extension DiscoverViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        for i in 1...20 {
            prices.append(PriceList(price: 225 * i))
        }
        
        return prices.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        cell.priceLabel.text = String(prices[indexPath.row].price!) + " TL"
        
        return cell
    }
    
}
