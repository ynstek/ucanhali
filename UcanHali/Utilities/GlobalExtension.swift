//
//  GlobalExtension.swift
//  UcanHali
//
//  Created by Yunus TEK on 22.09.2018.
//  Copyright © 2018 Yunus TEK. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func backButton() {
        navigationController?.navigationBar.topItem?.title = " " // x10 character
        navigationController?.navigationBar.tintColor = UIColor.black
    }
    
    func presentTransparentNavigationBar() {
        UIApplication.shared.statusBarView?.backgroundColor = UIColor("#EF2535")
 self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for:UIBarMetrics.default)
        self.navigationController!.navigationBar.isTranslucent = true
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.setNavigationBarHidden(false, animated:true)
//        self.navigationItem.hidesBackButton = true
        //        self.edgesForExtendedLayout = UIRectEdge.None // Kenarlar
        //        self.navigationController!.navigationBar.barTintColor = GlobalFunctions.shared.getColor("black")
    }
}

extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}

extension UIColor {
    // RGB
    static func colorWithRedValue(_ redValue: CGFloat, greenValue: CGFloat, blueValue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: redValue/255.0, green: greenValue/255.0, blue: blueValue/255.0, alpha: alpha)
    }
    
    // #Hex Color Code
    // UIColor("#E52320")
    convenience init(_ hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
