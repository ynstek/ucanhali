//
//  GlobalVariables.swift
//  UcanHali
//
//  Created by Yunus TEK on 22.09.2018.
//  Copyright © 2018 Emre Özdil. All rights reserved.
//

import Foundation
import UIKit

private let _sharedGlobalVariables = GlobalVariables()
class GlobalVariables : NSObject  {
    
    // MARK: - SHARED INSTANCE
    class var shared : GlobalVariables {
        return _sharedGlobalVariables
    }
    
    lazy var airports : [FareInfoModel]? = {
        return [FareInfoModel]()
    }()
    
    lazy var portList : [PortModel]? = {
        return [PortModel]()
    }()

}
